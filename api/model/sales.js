
const db = require('../../db/dbconnection')

const Sales = {
  list: function (limit, callback) {
    return db.query('select  * from sales order by id desc limit ' + limit, callback)
  },
  add: function (sale, callback) {
    const sql = `
      insert into sales (clientId, productId, price, date, paid) values (
        ${sale.clientId},
        ${sale.productId},
        ${sale.price},
        '${sale.date}',
        ${sale.paid}
      )`
    return db.query(sql, callback)
  },
  get: function (saleId, callback) {
    const sql = `select * from sales where id=${saleId}`
    return db.query(sql, callback)
  },
  update: function (sale, callback) {
    const sql = `update sales set clientId=${sale.clientId}, productId=${sale.productId}, price=${sale.price}, date="${sale.date}", paid=${sale.paid} where id=${sale.id};`
    console.log(sql)
    return db.query(sql, callback)
  }
}

module.exports = Sales
