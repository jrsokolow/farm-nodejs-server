const db = require('../../db/dbconnection')

const Product = {
  getAll: function (callback) {
    return db.query('select * from products', callback)
  }
}

module.exports = Product
