const db = require('../../db/dbconnection')

const Client = {

  list: function (callback) {
    return db.query('select * from clients', callback)
  }

}

module.exports = Client
