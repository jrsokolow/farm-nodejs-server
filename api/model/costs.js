const db = require('../../db/dbconnection')

const Costs = {
  list: function (limit, callback) {
    return db.query('select  * from costs order by id desc limit ' + limit, callback)
  },
  add: function (cost, callback) {
    const sql = `
      insert into costs (description, price, date) values (
        '${cost.description}',
        ${cost.price},
        '${cost.date}'
      )`
    return db.query(sql, callback)
  },
  get: function (costId, callback) {
    const sql = `select * from costs where id=${costId}`
    return db.query(sql, callback)
  },
  update: function (cost, callback) {
    const sql = `update costs set description=${cost.description}, price=${cost.price}, date=${cost.date} where id=${cost.id};`
    return db.query(sql, callback)
  }
}

module.exports = Costs
