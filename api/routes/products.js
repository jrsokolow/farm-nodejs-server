const express = require('express')
const router = express.Router()
const productsModel = require('../model/products')

router.get('/', (req, res, next) => {
  return productsModel.getAll((err, products) => {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      return res.json(products)
    }
  })
})

module.exports = router
