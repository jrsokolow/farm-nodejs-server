const express = require('express')
const router = express.Router()
const salesModel = require('../model/sales')

router.get('/', (req, res, next) => {
  const limit = req.query.limit || 5
  salesModel.list(limit, function (err, sales) {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      return res.json(sales)
    }
  })
})

router.post('/', (req, res, next) => {
  const sale = {
    clientId: req.body.clientId,
    productId: req.body.productId,
    price: req.body.price,
    date: req.body.date,
    paid: req.body.paid
  }
  salesModel.add(sale, (err, response) => {
    if (err) {
      console.log(err)
      return res.json({ error: 'Request failed' })
    } else {
      return res.json({ message: 'Sale added' })
    }
  })
})

router.get('/sale/:saleId', (req, res, next) => {
  salesModel.get(req.params.saleId, (err, response) => {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      if (response.length > 0) {
        let sale = response[0]
        sale.paid = sale.paid === 1
        return res.json(response[0])
      } else {
        return res.json({ message: 'Sale not found' })
      }
    }
  })
})

router.put('/sale', (req, res, next) => {
  console.log(req.body.date)
  const sale = {
    clientId: req.body.clientId,
    productId: req.body.productId,
    price: req.body.price,
    date: req.body.date,
    paid: req.body.paid,
    id: req.body.id
  }
  salesModel.update(sale, function (err, response) {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      return res.json({ message: 'Sale updated' })
    }
  })
})

module.exports = router
