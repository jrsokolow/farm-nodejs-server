const express = require('express')
const router = express.Router()
const costsModel = require('../model/costs')

router.get('/', (req, res, next) => {
  const limit = req.query.limit || 5
  costsModel.list(limit, function (err, sales) {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      return res.json(sales)
    }
  })
})

router.post('/', (req, res, next) => {
  const cost = {
    description: req.body.description,
    price: req.body.price,
    date: req.body.date
  }
  costsModel.add(cost, (err, response) => {
    if (err) {
      console.log(err)
      return res.json({ error: 'Request failed' })
    } else {
      return res.json({ message: 'Sale added' })
    }
  })
})

router.get('/cost/:costId', (req, res, next) => {
  costsModel.get(req.params.costId, (err, response) => {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      if (response.length > 0) {
        let cost = response[0]
        return res.json(cost)
      } else {
        return res.json({ message: 'Sale not found' })
      }
    }
  })
})

router.put('/cost', (req, res, next) => {
  const cost = {
    description: req.body.description,
    price: req.body.price,
    date: req.body.date
  }
  costsModel.update(cost, function (err, response) {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      return res.json({ message: 'Sale updated' })
    }
  })
})

module.exports = router
