const express = require('express')
const router = express.Router()
const clientsModel = require('../model/clients')

router.get('/', (req, res, next) => {
  return clientsModel.list((err, clients) => {
    if (err) {
      return res.json({ error: 'Request failed' })
    } else {
      return res.json(clients)
    }
  })
})

module.exports = router
