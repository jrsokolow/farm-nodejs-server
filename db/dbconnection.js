var mysql = require('mysql')

var connection = mysql.createPool({

  host: process.env.FARM_DBHOST,
  user: process.env.FARM_DBUSER,
  password: process.env.FARM_DBPASS,
  database: process.env.FARM_DBSCHEMA

})

module.exports = connection
